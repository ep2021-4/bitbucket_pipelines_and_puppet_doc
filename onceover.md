# Testing Puppet Control Repositories with Onceover and Bitbucket Pipelines

## Puppet Control Repositories

Puppet Control Repositories bind together:

* [Hiera](https://puppet.com/docs/puppet/5.3/hiera_intro.html) User data
* The modules to install on a Puppet Master, described in a [Puppetfile](https://puppet.com/docs/pe/2017.2/cmgmt_puppetfile.html)
* [Roles and Profiles](https://puppet.com/docs/pe/2017.2/r_n_p_intro.html) to use at a given customer site
* [Node definitions](https://puppet.com/docs/puppet/5.3/lang_node_definitions.html) attaching specific resources to nodes

_With Bitbucket Pipelines, we can test and deploy code for every `git push`._


## Onceover
Testing Puppet Control repositories has traditionally been more involved then the testing of individual modules with folks resorting to manual tests or complex one-off build systems.  Fortunately, we now have [Onceover](https://github.com/dylanratcliffe/onceover) which exists to get organisations up and running quickly in terms of being able to test the quality of their Puppet Control Repository code.

Getting started with onceover is a simple process:

```shell

cd /your/control/repository

# Main installation
gem install onceover

# Generate initial configuration
onceover init

# Bundler-compatible installation (to allow multiple gem versions on a system - just do it)
bundle install

# Execute the tests (RSpec-Puppet)
bundle exec onceover run spec
```

The exit status of `onceover run spec` indicates whether the tests passed or failed.  If all tests pass, then the code is ready to be deployed to the Puppet Master and this action is triggered by making a request to the Code Manager API using `curl`.


## Configuring Bitbucket Pipelines
Most of the configuration needed for Bitbucket Pipelines has already been done for you, so you just need to download a copy into your own repository.

Following the steps below will give you:

* Syntax checking
* Linting
* Onceover (RSpec-Puppet)
* Mocks (Stub modules giving ability to test code that interacts with Puppet Enterprise)

### Step 1: Obtain files
`cd` into your checked out copy of the control repository, then download a copy of the following files:

* [Makefile](https://bitbucket.org/geoff_williams/my_puppet_control_repo/src/production/Makefile?at=production&fileviewer=file-view-default)
* [Gemfile](https://bitbucket.org/geoff_williams/my_puppet_control_repo/src/production/Gemfile?at=production&fileviewer=file-view-default)
* [Puppetfile.mock](https://bitbucket.org/geoff_williams/my_puppet_control_repo/src/production/Puppetfile.mock?at=production&fileviewer=file-view-default)
* [bitbucket-pipelines.yml](https://bitbucket.org/geoff_williams/my_puppet_control_repo/src/production/bitbucket-pipelines.yml?at=production&fileviewer=file-view-default)
* [puppet_code_manager.sh](https://bitbucket.org/geoff_williams/my_puppet_control_repo/src/production/puppet_code_manager.sh?at=production&fileviewer=file-view-default)

`puppet_code_manager.sh` needs to be made executable in the repository.

_If you have a Linux machine, you can do all of this easily from a terminal:_

```shell
wget https://bitbucket.org/geoff_williams/my_puppet_control_repo/raw/production/Makefile
wget https://bitbucket.org/geoff_williams/my_puppet_control_repo/raw/production/Gemfile
wget https://bitbucket.org/geoff_williams/my_puppet_control_repo/raw/production/Puppetfile.mock
wget https://bitbucket.org/geoff_williams/my_puppet_control_repo/raw/production/bitbucket-pipelines.yml
wget https://bitbucket.org/geoff_williams/my_puppet_control_repo/raw/production/puppet_code_manager.sh
chmod +x puppet_code_manager.sh
```

### Step 2: Configure Puppet Enterprise

1. [Enable Code Manager](https://docs.puppet.com/pe/latest/code_mgr_config.html#enable-code-manager)
2. [Create a new RBAC user and set a password](https://docs.puppet.com/pe/latest/rbac_user_roles.html#create-a-new-user)
3. [Add the new user to the `Code Deployers` RBAC role](https://docs.puppet.com/pe/latest/rbac_user_roles.html#add-a-user-to-a-user-role)
4. [Generate an RBAC token](https://docs.puppet.com/pe/latest/rbac_token_auth.html#generating-a-token-for-use-by-a-service)

_These steps can be automated using the [ncedit](https://github.com/declarativesystems/ncedit) and [pe_rbac](https://github.com/declarativesystems/pe_rbac) gems_

### Step 3: Obtain authentication tokens and server details from the Puppet Master

* Contents of the CA Certificate, available at `/etc/puppetlabs/puppet/ssl/certs/ca.pem`
* The configured certname, available by running `puppet config print certname`
* The IP address or public DNS address of the Puppet Master
* The contents of the RBAC authentication token you created

### Step 4: Add environment variables to Bitbucket Pipelines
From the main Bitbucket page for your repository, open the `Environment variables` screen:

1. Click settings
2. Scroll down to `Pipelines`
3. Click `Environment variables`


Now enter the details you collected earlier:

* `PE_CA_CERT_PEM` - Copy and paste the contents of your CA certificate
* `PE_PUPPET_SERVER_CERTNAME` - the certname of your puppet master
* `PE_RBAC_TOKEN` - Paste the contents of your RBAC token - be sure to select the `secured` option when entering this as your token is a valuable security credential
* `PE_PUPPET_SERVER_ADDRESS` - Public DNS name of your puppet master (if different to `PE_SERVER_CERTNAME`)
* `DEBUG_DEPLOY` - (optional) If you would like to run the deployment script in bash debug mode, set this variable to true
![Environment variables](images/environment_variables.png)

That's it - next time you push code to the repository you should see it being deployed to your Puppet Master.

## Worked Example
You can see a complete working example at [https://bitbucket.org/geoff_williams/my_puppet_control_repo](https://bitbucket.org/geoff_williams/my_puppet_control_repo)

## Onceover Docker image
The above image is not maintained and is based on the official `Ruby` image.  It caches many of onceover's dependecies affording for much faster test execution.  Dylan Ratcliffe, the author of Onceover, might like to take over owernship of this image, which could then be built as part of onceover's release process - stay tuned.

In the meantime:

* If you experience problems with the above image bug reports are welcome at [docker-onceover](https://github.com/geoffwilliams/docker-onceover), although there is no expectation of support
* If you need a newer version of Onceover, specify it in your `Gemfile` and it will be used automatically (you can speed up test execution by building your own image)

## Troubleshooting

* Make sure you spelt the hostname and IP address correctly
* Ensure that TCP port 8170 to your puppet master is open
* Be sure that authentication tokens were copied and pasted correctly
* Make sure you are accessing the Puppet Master using the correct certname
* As a last resort, you can disable SSL certificate verification by setting the Environment Variable `FORCE_INSECURE_CONNECTION` to true.  Note that this exposes you to impersonation attacks.
* To debug a failed build, you can create an identical container by running:

```shell
docker run -ti --entrypoint=/bin/bash "geoffwilliams/onceover:2017-09-22-0"
```

* Use a volume or checkout code inside the container to test the whole build process
